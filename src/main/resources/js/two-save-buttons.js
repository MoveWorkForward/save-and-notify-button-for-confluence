/*global AJS, TwoSaveButtons, Confluence */
AJS.bind('init.rte', function () {
    'use strict';

    var $ = AJS.$;
    var checkbox = $('input#notifyWatchers');
    var savebutton = $('button#rte-button-publish');
    var parent = savebutton.parent();
    var initialized = false;

    if (checkbox.is(':visible')) {
        if (parent.is('li.toolbar-item')) { // Confluence 5.1
            parent.before(TwoSaveButtons.secondButtonLi());
        } else if (parent.is('div.save-button-container')) {
            var isButtonWithUpdateLabel = (savebutton.text() === TwoSaveButtons.firstButton_UpdateVersionLabel()); 
            if( isButtonWithUpdateLabel ){  //Confluence 6.0 with Synchrony enabled
                savebutton.before(TwoSaveButtons.secondButton_UpdateVersion());             
            }else{          
                savebutton.before(TwoSaveButtons.secondButton());
            }           
        }
        savebutton.removeClass('aui-button-primary');
        checkbox.parent().hide();

        savebutton.click(function() {
            if (!initialized) {
                checkbox.attr('value', false); // for old versions
                checkbox.attr('checked', false); // for old versions
                checkbox[0].checked = false; // needed for Confluence 5.8 upwards
                initialized = true;
            } // then the event bubbles up and triggers the save
        });

        $('button#rte-button-publish-and-notify').click(function () {
            checkbox.attr('value', true);
            checkbox.attr('checked', 'checked');
            checkbox[0].checked = true;
            initialized = true;
            savebutton.click();
            return false; // only the savebutton should bubble up
        });
    }
});
